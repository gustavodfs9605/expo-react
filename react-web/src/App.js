import React from 'react';

import './CSS/master.scss' ;

import Header from './components/header';
import Indice from './components/indice';
import Presentacion from './components/presentacion';

class App extends React.Component {

  constructor(props) {
    super (props);
    this.state = {
      indice : -1
    }
    this.cambiarIndice = this.cambiarIndice.bind(this);
  }

  cambiarIndice(v){
    this.setState({indice: v});
  }

  render(){

    const indice = this.state.indice; 

    return(
      <div className="App">
        <Header/>

        <div className="Contenedor">
          <Indice className="Indice" indice={indice} cambiarIndice={this.cambiarIndice}/> 
          <Presentacion className="Contenido" indice={indice} cambiarIndice={this.cambiarIndice}/>
        </div>
        
      </div>

    );
  }
}

export default App;
