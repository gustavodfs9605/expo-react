import React, { Component } from 'react';
import Node from './node';

import '../CSS/master.scss'

class Nodes extends Component {


    state = { 
        nodes: [
            { id: 1, value: 0 },
            { id: 2, value: 0 },
            { id: 3, value: 0 },
        ]
     }

    render() { 
        return ( <div className="nodes-container">
           { this.state.nodes.map( node => <Node key={node.id} /> ) }
        </div> );
    }
}
 
export default Nodes;