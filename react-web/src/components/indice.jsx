import React, { Component } from 'react';
import '../CSS/indice.scss';

class Indice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valor : 0
        };
        this.handleClick = this.handleClick.bind(this)
    }


    handleClick(e){
        this.props.cambiarIndice(e);
    }
    
    render(){
        return(
            <div>
                {this.crearIndice()}
            </div>
        );
    }



    crearIndice(){
        return(
            <div className="indice">
                <ol>
                    <li>
                        <div onClick={e=>this.handleClick(-1)} className="enlace">
                            Introducción
                        </div>
                    </li>
                    <ol>
                        <li>
                            <div onClick={e=>this.handleClick(0)} className="enlace">
                            ¿Qué es react?
                            </div>
                        </li>
                        <li><div onClick={e=>this.handleClick(1)} className="enlace">Dom y Virtual Dom</div></li>
                    </ol>
                    <li><div onClick={e=>this.handleClick(2)} className="enlace">Instalación</div></li>
                    <ol>
                        <li><div onClick={e=>this.handleClick(3)} className="enlace">Enlaces CDN</div></li>
                        <li><div onClick={e=>this.handleClick(4)} className="enlace">Node</div></li>
                    </ol>
                    <li><div onClick={e=>this.handleClick(5)} className="enlace">Conceptos Basicos</div></li>
                    <ol>
                        <li><div onClick={e=>this.handleClick(6)} className="enlace">JSX</div></li>
                        <li><div onClick={e=>this.handleClick(7)} className="enlace">ECMAScript v6</div></li>
                        <ol>
                            <li><div onClick={e=>this.handleClick(8)} className="enlace">Clases</div></li>
                            <li><div onClick={e=>this.handleClick(9)} className="enlace">Funciones Flecha</div></li>
                            <li><div onClick={e=>this.handleClick(10)} className="enlace">Otros</div></li>
                        </ol>                            
                    </ol>
                    <li><div onClick={e=>this.handleClick(11)} className="enlace">Hola Mundo!! en React</div></li>
                    <li><div className="enlace"> Componentes </div></li>
                    <ol>
                        <li><div onClick={e=>this.handleClick(12)} className="enlace"> Componentes por funciones</div></li>
                        <li><div onClick={e=>this.handleClick(13)} className="enlace"> Componentes por clases</div></li>
                    </ol>
                    <li><div className="enlace"> Ejemplo Sencillo y Bonito. </div></li>
                    <ol>
                        <li><div onClick={e=>this.handleClick(12)} className="enlace">Zum Beispiel.</div></li>
                    </ol>

                    <li><div onClick={e=>this.handleClick(30)} className="enlace"> Ejemplo </div></li>
                </ol>
            </div>
        );
    }
}

export default Indice;