import React, { Component } from 'react';

// import logo from '../logo.svg';
import logo from '../IMGS/blue-logo.png';
import '../App.scss';

class Header extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <div className="title" >
                        React
                    </div>
                </header>
            </div>
         );
    }
}
 
export default Header;