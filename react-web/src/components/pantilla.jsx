import React, { Component } from 'react';

import '../CSS/plantilla.scss';

class Plantilla extends Component{

    

    render(){

        const titulo = this.props.titulo;
        const parrafo = this.props.parrafo;
        const codigo = this.props.codigo;
        const imagen = this.props.imagen;

        return (
           <div className="plantilla">
                <h1>{titulo}</h1>
                <div className={(!codigo)?"informacion":"portada2"}>
                    <p>
                        {parrafo}
                    </p>
                    {this.contieneCodigo(codigo,imagen) }
                </div>
           </div> 
        );
    }

    contieneCodigo( bandera,imagen ){
        console.log(bandera);
        if(bandera){
            return (<div>{imagen}</div>);
        }
        return(<img src={imagen}/>);
    }

}

export default Plantilla;