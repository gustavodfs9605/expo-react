import React, { Component } from 'react';

import '../CSS/presentacion.scss';
import '../CSS/master.scss';


import imgReact from '../IMGS/blue-logo.png';
import imgNode from '../IMGS/node.png';
import imgNPM from '../IMGS/npm.jpg';
import pres1 from '../IMGS/react1.png';
import pres2 from '../IMGS/dom.jpeg';
import imgJsx from '../IMGS/jsxEs.png'
import imgEs from '../IMGS/es6.jpeg';


import Plantilla from './pantilla';
import Nodes from './nodes';

class Presentacion extends Component {

    constructor( props) {
        super(props);
        this.informacion = [{
                    titulo: '¿Qué es React?' ,
                    codigo: false,
                    parrafo: 'React es una biblioteca escrita en JavaScript, desarrollada en Facebook para facilitar la creación de componentes interactivos, reutilizables, para interfaces de usuario.',
                    imagen: pres1
                },{
                    titulo: 'Virtual dom y dom' ,
                    codigo: false,
                    parrafo: 'El secreto de ReactJS para tener un performance muy alto, es que implementa algo llamado Virtual DOM y en vez de renderizar todo el DOM en cada cambio, que es lo que normalmente se hace, este hace los cambios en una copia en memoria y después usa un algoritmo para comparar las propiedades de la copia en memoria con las de la versión del DOM y así aplicar cambios exclusivamente en las partes que varían.',
                    imagen: pres2
                },{
                    titulo: 'Instalación',
                    codigo: false,
                    imagen: imgReact,
                },{
                    titulo: 'Enlaces CDN',
                    codigo: true,
                    parrafo: 'Instalación por medio de CDN',
                    imagen: <iframe width="100%" height="100px" src="//jsfiddle.net/gustavodfs9605/voxweb6L/embedded/js/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Node y npm',
                    codigo: true,
                    parrafo: 'Instalación por medio de Node y npm',
                    imagen: <iframe width="100%" height="300px" src="//jsfiddle.net/gustavodfs9605/voxweb6L/1/embedded/js/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Conceptos basicos',
                    imagen: imgJsx
                },{
                    titulo: 'JSX' ,
                    codigo: true ,
                    parrafo: 'JSX es una extensión de JavaScript y es recomendable utilizarlo ya que permite crear elementos react con una sintaxis muy parecida al html. JSX permite incrustar codigo de javaScript, utilizando llaves',
                    imagen: <iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/voxweb6L/3/embedded/js/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

                },{
                    titulo: 'ECMAScript v6',
                    imagen: imgEs
                },{
                    titulo: 'clases',
                    codigo: true,
                    parrafo: 'Ahora JavaScript tendrá clases, muy parecidas las funciones constructoras de objetos que realizamos en el estándar anterior, pero ahora bajo el paradigma de clases, con todo lo que eso conlleva, como por ejemplo, herencia.',
                    imagen: <iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/voxweb6L/11/embedded/js/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Funciones flecha',
                    codigo: true,
                    parrafo: 'Básicamente las funciones flechas son funciones anónimas pero su sintaxis es más limpia.',
                    imagen: <iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/voxweb6L/4/embedded/js/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Otros',
                    codigo: true,
                    parrafo: 'Palabra reservada this, variables let y const',
                    imagen: <iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/voxweb6L/17/embedded/js/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Hola Mundo!! en React',
                    codigo: true,
                    imagen:<iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/05u43ena/embedded/js,html,result/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Componentes por funciones',
                    codigo: true,
                    parrafo: 'Un componente puede ser una función.Ejemplo...',
                    imagen: <iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/05u43ena/6/embedded/js,html,result/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                },{
                    titulo: 'Componentes por clases',
                    codigo: true,
                    parrafo: 'Ejemplo...',
                    imagen: <iframe width="100%" height="300" src="//jsfiddle.net/gustavodfs9605/05u43ena/23/embedded/js,html,result/dark/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
                }

            ];
    }


    render(){
        const indice = this.props.indice;
        return(
            <div>
                {this.esPlantilla(indice)}
            </div>
        );      
    }


    esPlantilla( n ){
        if(n === -1 ){
            return (
                <div className="portada">
                    <img src={imgReact} alt="" className="react"/>
                    <img src={imgNode} alt="" className="node"/>
                    <img src={imgNPM} alt="" className="npm"/>
                </div>
            );
        }
        if(n === 30){
            return <Nodes className="Contenido"/> ;
        }
        return (
            <Plantilla 
                titulo = {this.informacion[n].titulo}
                parrafo =  {this.informacion[n].parrafo}
                imagen = {this.informacion[n].imagen}
                codigo = {this.informacion[n].codigo}
            />
        );
    }
    
}

export default Presentacion;