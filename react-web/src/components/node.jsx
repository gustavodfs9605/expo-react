import React, { Component } from 'react';

import '../CSS/node.scss'
import gif from '../IMGS/blue-logo.png';
import 'bootstrap/dist/css/bootstrap.css';


// import 'bootstrap/dist/css/bootstrap.css';

class Node extends Component {

    state = { 
        count: 0,
        flag: false
        // nodes: ["node1","node2","node3"]
    }

    constructor(){
        super();
        this.increment =  this.increment.bind(this);
    }

    style ={
        fontSize: 40,
        fontWeight: "bold"
    }

    render() { 

        return(
            <div>
                 <div  onClick={this.handleClick} data-toggle="tooltip" title="Aujourd'hui" className="node-container" >
                    <div className="innerCircle">
                        <div className="centerCircle">
                            <img src={gif} alt="react-gif"/>
                        </div>
                    </div>
                </div>

                {/* <div onClick={this.showNodes} className="info-container">
                    <div className="title-container">
                        <img src={gif} alt="little-gif"/>
                        <h2>{this.state.title}</h2>
                    </div>
                    <div className="innerSquare">
                    </div>
                </div> */}

                <div className="small-window">
                    <span style={this.style} className={this.changeClassDinamically()} >{this.printCount()}</span>
                    <button onClick={this.increment} type="button" className="mybtn1 btn btn-primary" >increment</button>
                    <button onClick={this.decrement} type="button" className="mybtn2 btn btn-secondary" >decrement</button>
                    {/* <ul>
                        {this.state.nodes.map( node => <li>{node}</li> )}
                    </ul> */}
                </div>

            </div>
           
        );
    }

    increment = () =>{
        // this.state.count++;
        this.setState( {count: this.state.count + 1} );
    }

    decrement = () =>{
        // this.state.count++;
        this.setState( {count: this.state.count - 1} );
    }

    printCount = () =>{
        let { count} = this.state;
        return count  === 0 ? <h1 style={this.style} >Z E R 0</h1> : count;
    }


    changeClassDinamically() {
        let classes = "mybagde1 m-1 badge-";
        classes+= this.state.count === 0 ? "warning" : "primary";
        return classes;
    }

    handleClick = () =>{

        if (this.state.flag === false) {
            let infoContainer = document.querySelectorAll('.small-window');
                for (let index = 0; index < infoContainer.length; index++) {
                    infoContainer[index].style.display = "grid";
                }
            this.setState( { flag: true } );

        } else{

            let infoContainer = document.querySelectorAll('.small-window');
                for (let index = 0; index < infoContainer.length; index++) {
                    infoContainer[index].style.display = "none";
                }
            this.setState( { flag: false } );

        }
    }

    componentDidMount() {
        let infoContainer = document.querySelectorAll('.small-window');
        for (let index = 0; index < infoContainer.length; index++) {
            infoContainer[index].style.display = "none";
        }
     }

}
 
export default Node;